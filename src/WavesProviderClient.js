'use strict';

const request = require('request-promise');

/**
 * Client for interacting with Swell's Waves provider API
 * @param options object { agentKey, production:optional }
 * @param options.agentKey string
 * @param options.production boolean optional default: true
 */
const WavesProviderClient = class WavesProviderClient {
  constructor(options) {
    if (!options.agentKey) {
      throw new Error('WavesProviderClient.constructor: options.agentKey must be set.');
    }
    this.agentKey = options.agentKey;
    this.baseUrl = 'https://api-provider.waves.ai/production';
    if (options.production === false) {
      this.baseUrl = 'https://api-provider-staging.waves.ai/staging';
    }
  }
  /**
   * Get a new Swell
   * @param options { chatBotUserId, tags, labels, useNonPaid }
   * @param options.chatBotUserId string
   * @param options.tags string optional
   * @param options.labels string optional
   * @param options.useNonPaid boolean optional default: true
   */
  getSwell(options) {
    if (!options.chatBotUserId) {
      throw new Error('WavesProviderClient.getSwell: options.chatBotUserId must be set.');
    }
    return request({
      url: `${this.baseUrl}/swell`,
      qs: {
        agentKey: this.agentKey,
        chatBotUserId: options.chatBotUserId,
        tags: options.tags,
        labels: options.labels,
        useNonPaid: options.useNonPaid,
      },
      json: true,
      method: 'GET',
    })
      .then((response) => {
        return response.swell;
      });
  }
  /**
   * Vote for a Swell by id
   * @param options { chatBotUserId, swellId, vote }
   * @param options.chatBotUserId string
   * @param options.swellId string
   * @param options.vote string possible values: 'a', 'b', swell.photos.[a|b].id
   */
  voteSwell(options) {
    if (!options.chatBotUserId) {
      throw new Error('wavesController.voteSwell: options.chatBotUserId must be set.');
    }
    if (!options.swellId) {
      throw new Error('wavesController.voteSwell: options.swellId must be set.');
    }
    return request({
      url: `${this.baseUrl}/swell/${options.swellId}/vote/${options.vote}`,
      qs: {
        agentKey: this.agentKey,
        chatBotUserId: options.chatBotUserId,
      },
      json: true,
      method: 'POST',
    });
  }
  /**
   * Update or create an user at Swell's storage
   * @param options { chatBotUserId, userData }
   * @param options.chatBotUserId string
   * @param options.userData object
   */
  updateUser(options) {
    if (!options.chatBotUserId) {
      throw new Error('wavesController.updateUser: options.chatBotUserId must be set.');
    }
    return request({
      url: `${this.baseUrl}/user`,
      qs: {
        agentKey: this.agentKey,
        chatBotUserId: options.chatBotUserId,
      },
      body: options.userData,
      json: true,
      method: 'POST',
    });
  }
};

module.exports = WavesProviderClient;
