## Swell Waves client API documentation

The Swell Waves client is a Javascript class and is to interact (and benefit) with our Swells over an API! You can **get** a Swell object, including i.e. the combined image and the question, **vote** for the Swell by either sending the strings  'a', 'b' or the image's id to vote for as a parameter's value and **update** a user in our database, which is in fact an update to your income!

In this short tutorial, we will quick-start you in using our lightweight client!

### initialization:

First, initialize the class with an ```agentKey```. To get an agentKey, [request one](mailto:phil@swell.wtf?subject=Signup%20for%20waves.ai%20as%20chatbot) by proving your chatbot platform and your bot's url (i.e. https://m.me/swell.bot).

At initialization, you can provide a ```production``` property, which defaults to true. Set this to false to send your requests to our staging environment. This might come handy for you while you are developing your bot.

```javascript
/**
 * @param options object { agentKey, production:optional }
 * @param options.agentKey string
 * @param options.production boolean optional default: true
 */
```

Example usage code:

```javascript
const waves = new WavesProviderClient({
  agentKey: AGENTKEY_YOU_GET_FROM_US,
  production: STAGE === 'production',
});
```

### getSwell:

To get a new Swell, provide the initiating user's id as ```chatBotUserId``` and optionally ```tags``` or ```labels``` for filtering our Swells and optionally the ```useNonPaid``` flag for accessing every Swell in our database as properties of ```getSwell()```'s first parameter.
 
In return, you will get an object holding the Swells's id, the question, the pictures and the creation timestamp as a promise's value.

```javascript
/**
* @param options { chatBotUserId, tags, labels, useNonPaid }
* @param options.chatBotUserId string
* @param options.tags string optional
* @param options.labels string optional
* @param options.useNonPaid boolean optional
* @return Promise Promise holding a Swell object
*/
```

Example usage code:

```javascript
waves.getSwell({
  chatBotUserId: user.id,
})
  .then((swell) => {
    // store Swell id & link to result image for later use
    db.save('swellId', swell.id);
    db.save('swellResult', swell.images.combined_result);
    // send Swell rich-card
    createCard({
      title: swell.question,
      image_url: swell.images.combined,
    });
  })
  .catch((error) => {
    console.error(`error while fetching swell: ${error.message || error}`);
  });
```

### voteSwell:

To vote for one of the two images of a previously received Swell, provide again the initiating user's id as ```chatBotUserId```, the Swell's previously saved id as ```swellId``` and either the string 'a', 'b' or the image's id as ```vote``` as properties of ```voteSwell()```'s first parameter.

```javascript
/**
* @param options { chatBotUserId, swellId, vote }
* @param options.chatBotUserId string
* @param options.swellId string
* @param options.vote string possible values: 'a', 'b', swell.photos.[a|b].id
*/
```

Example usage code:

```javascript
waves.voteSwell({
  chatBotUserId: user.id,
  swellId: db.load('swellId'), // use previously saved Swell id
  vote: userChoice, // must be 'a', 'b,' or the photo's id
})
  .then(() => {
    // send result image of voted Swell 
    createAttachmentMessage({
      url: db.load('swellResult'), // use previously saved Swell result url
      type: 'image/jpg',
    });
  })
  .catch((error) => {
    console.error(`error while voting for swell: ${error.message || error}`);
  });
```

### updateUser:

To create or update an user on our server, provide the user's id as ```chatBotUserId``` along with an object that holds relevant user data as ```userData```  as properties of ```updateUser()```'s first parameter [[more info here](http://provider-apidoc.waves.ai/#api-User-update_user)].

You will get an excerpt of the user's data as a promise's value.

```javascript
  /**
   * @param options { chatBotUserId, userData }
   * @param options.chatBotUserId string
   * @param options.userData object
   * @return Promise holding an User object
   */
```

Example usage code:
  
```javascript
// get public profile from Facebook
request({
  url: `https://graph.facebook.com/v2.6/${user.id}?fields=first_name,last_name,locale,timezone,gender`,
  qs: { access_token: FACEBOOK_PAGE_TOKEN },
  method: 'GET',
})
  .then((data) => {
    return waves.updateUser({
      chatBotUserId: user.id,
      userData: JSON.parse(data),
    });
  })
  .catch((error) => {
    console.error('error while updating user:', error.message || error);
  });
```
